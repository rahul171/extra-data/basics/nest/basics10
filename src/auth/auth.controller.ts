import { Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { User } from '../common/decorators/user.decorator';
import { User as UserType } from '../users/user.types';
import { Token } from './interfaces/token.interface';
import { TokenVerifyGuard } from './guards/token-verify.guard';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { AuthGuard } from '@nestjs/passport';
import { GoogleOauth20Guard } from './guards/google-oauth20.guard';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  // register globally instead of registering here.
  // @UseFilters(GeneralExceptionsFilter)
  @Post('login')
  login(@User() user: UserType) {
    return { user };
  }

  @UseGuards(LocalAuthGuard)
  @Post('login-token')
  async loginToken(@User() user: UserType): Promise<Token> {
    return this.authService.generateToken(user);
  }

  @UseGuards(TokenVerifyGuard)
  @Post('need-token')
  needToken() {
    return '/need-token is accessible';
  }

  @UseGuards(JwtAuthGuard)
  @Post('need-token-passport-jwt')
  needTokenPassportJwt(@User() user) {
    return { user };
  }

  @UseGuards(GoogleOauth20Guard)
  @Get('google')
  googleOauth20URL() {}

  @UseGuards(GoogleOauth20Guard)
  @Get('google/callback')
  googleOauth20Callback(@User() user) {
    return { user };
  }
}
