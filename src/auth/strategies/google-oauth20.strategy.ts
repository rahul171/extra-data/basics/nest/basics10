import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-google-oauth20';

@Injectable()
export class GoogleOauth20Strategy extends PassportStrategy(
  Strategy,
  'google',
) {
  constructor() {
    super({
      clientID:
        '194800263826-of3daj228u1mo3imtj55pgnkmphub0cj.apps.googleusercontent.com',
      clientSecret: 'bEQPK32-9x9aRlY2xasr3fRQ',
      callbackURL: 'http://localhost:3000/auth/google/callback',
      scope: ['email', 'profile'],
    });
  }

  async validate(accessToken, refreshToken, profile, callback) {
    return { accessToken, refreshToken, profile };
  }
}
