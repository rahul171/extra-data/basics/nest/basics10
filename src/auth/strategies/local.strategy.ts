import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { AuthService } from '../auth.service';
import { AuthException } from '../../common/exceptions/handlers/auth.exception';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super();
  }

  async validate(username: string, password: string) {
    // if these exact parameters are not present in the request body, then this
    // method won't get called and passport will throw an exception.
    // to change the variable name that passport is looking for, pass
    // options in the super() call. for eg. super({ usernameField: 'email' })

    const user = await this.authService.validateUser(username, password);

    if (!user) {
      throw new AuthException();
    }

    return user;
  }
}
