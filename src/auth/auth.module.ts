import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { LocalStrategy } from './strategies/local.strategy';
import { UsersModule } from '../users/users.module';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from '../common/config/jwt-constants';
import { JwtStrategy } from './strategies/jwt.strategy';
import { GoogleOauth20Strategy } from './strategies/google-oauth20.strategy';

@Module({
  imports: [
    UsersModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: {
        expiresIn: '60s',
      },
    }),
  ],
  providers: [AuthService, LocalStrategy, JwtStrategy, GoogleOauth20Strategy],
  controllers: [AuthController],
})
export class AuthModule {}
