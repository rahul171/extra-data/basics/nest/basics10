import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { User } from '../users/user.types';
import { JwtService } from '@nestjs/jwt';
import { Token } from './interfaces/token.interface';

@Injectable()
export class AuthService {
  constructor(
    private userService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(
    username: string,
    password: string,
  ): Promise<User | boolean> {
    const user = await this.userService.findOne(username);
    if (user && user.password === password) {
      return user;
    }
    return false;
  }

  async generateToken(user: User): Promise<Token> {
    const payload = {
      username: user.username,
      name: user.name,
    };
    return {
      token: this.jwtService.sign(payload),
    };
  }

  verifyToken(token) {
    return this.jwtService.verify(token);
  }
}
