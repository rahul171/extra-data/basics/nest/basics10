import { ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthException } from '../../common/exceptions/handlers/auth.exception';

@Injectable()
export class GoogleOauth20Guard extends AuthGuard('google') {
  canActivate(context: ExecutionContext) {
    return super.canActivate(context);
  }

  handleRequest(err, user, info) {
    if (err || !user) {
      throw new AuthException('Invalid google authentication');
    }
    return user;
  }
}
