import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Request } from 'express';
import { AuthService } from '../auth.service';
import { AuthException } from '../../common/exceptions/handlers/auth.exception';

@Injectable()
export class TokenVerifyGuard implements CanActivate {
  constructor(private authService: AuthService) {}

  canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest<Request>();
    const headerTokenString = request.header('Authorization');

    if (!headerTokenString) {
      return false;
    }

    const token = headerTokenString.replace('Bearer ', '');

    let payload;

    try {
      payload = this.authService.verifyToken(token);
    } catch (err) {
      throw new AuthException('Invalid Token, please log in again');
    }

    return payload;
  }
}
