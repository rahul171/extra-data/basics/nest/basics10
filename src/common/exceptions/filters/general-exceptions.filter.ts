import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { GeneralException } from '../handlers/general.exception';
import { Response } from 'express';

@Catch(GeneralException)
export class GeneralExceptionsFilter implements ExceptionFilter {
  catch(exception: GeneralException, host: ArgumentsHost) {
    const message = exception.getResponse();
    const status = exception.getStatus();

    const responseObj = this.getResponseMessage(message, status);

    const response = host.switchToHttp().getResponse<Response>();

    response.status(status).json(responseObj);
  }

  private getResponseMessage(message: string | object, status: number) {
    return {
      error: {
        message,
      },
      httpStatus: status,
    };
  }
}
