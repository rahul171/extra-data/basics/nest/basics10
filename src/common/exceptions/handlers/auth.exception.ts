import { HttpStatus } from '@nestjs/common';
import { GeneralException } from './general.exception';

export class AuthException extends GeneralException {
  constructor(message?: any) {
    message = message || 'AuthException => Authentication Failed';
    super(message, HttpStatus.UNAUTHORIZED);
  }
}
