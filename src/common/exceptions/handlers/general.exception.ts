import { HttpException, HttpStatus } from '@nestjs/common';

export class GeneralException extends HttpException {
  constructor(message: string, status: HttpStatus) {
    super(message, status);
  }
}
