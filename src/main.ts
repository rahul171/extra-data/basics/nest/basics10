import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AppModule } from './app.module';
import { GeneralExceptionsFilter } from './common/exceptions/filters/general-exceptions.filter';

const bootstrap = async () => {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.useGlobalFilters(new GeneralExceptionsFilter());
  await app.listen(3000);
};

bootstrap();
