import { Injectable } from '@nestjs/common';
import { User } from './user.types';

@Injectable()
export class UsersService {
  private users: User[];

  constructor() {
    this.users = [
      {
        name: 'name1',
        username: 'username1',
        password: 'password1',
        message: 'message1',
      },
      {
        name: 'name2',
        username: 'username2',
        password: 'password2',
        message: 'message2',
      },
      {
        name: 'name3',
        username: 'username3',
        password: 'password3',
        message: 'message3',
      },
      {
        name: 'name4',
        username: 'username4',
        password: 'password4',
        message: 'message4',
      },
      {
        name: 'name5',
        username: 'username5',
        password: 'password5',
        message: 'message5',
      },
      {
        name: 'name6',
        username: 'username6',
        password: 'password6',
        message: 'message6',
      },
    ];
  }

  async getUsers(): Promise<User[]> {
    return this.users;
  }

  async findOne(username: string): Promise<User> {
    return this.users.find(user => user.username === username);
  }
}
